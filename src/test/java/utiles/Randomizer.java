package utiles;

import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class Randomizer {



    public static String month(){
        int number = faker.number().numberBetween(1,12);
        if (number < 10){
            return String.format("0%d",number);
        }
        else{
            return String.format("%d",number);
        }
    }







    private static Random random = new Random();
    private static Faker faker = new Faker(new Locale("pl"));

    public static Integer inRange(Integer from, Integer to) {
        return random.nextInt(to + 1 - from) + from;
    }

    static String firstName = faker.name().firstName();
    static String lastName = faker.name().lastName();


    public static String word(){
        return faker.lorem().word();

    }

    public static String firstName(){
        return firstName;
    }

    public static String lastName(){
        return lastName;
    }

    public static int number(){
        return faker.number().numberBetween(1,9);
    }

    public static String email(){

        return String.format("%s.%s%d@test.com",firstName(),lastName(),number());


    }

    public static String fullAdress(){
        return String.format("%s", faker.address().streetAddress());
    }

    public static String postcode(){
        return faker.address().zipCode();
    }

    public static String city(){
        return faker.address().city();
    }

    //String streetName = faker.address().streetName();
    //String number = faker.address().buildingNumber();
    //String city = faker.address().city();

    public static String chuckNorrisFact(){
        return faker.chuckNorris().fact();
    }
    public static String phoneNumber(){
        return faker.phoneNumber().phoneNumber();
    }

    public static String password(){
        return faker.internet().password(8,10,true,false,true);
    }

    public static String cvccode(){
        return String.format("%d",faker.number().numberBetween(100,999));
    }

    public static String expiriedDate(){
        return String.format("%s%s",month(),faker.number().numberBetween(20,99));
    }


}


