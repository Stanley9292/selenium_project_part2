package utiles;

import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IframeMenager {

    public static WebDriver driver = DriverManager.getWebDriver();

    public void changeIframe(String name) {
        driver.switchTo().frame(name);
    }

    public void changeIframe(int number) {
        driver.switchTo().frame(number);
    }

    public void changeIframe(WebElement webElement) {
        driver.switchTo().frame(webElement);
    }

    public void returnToDefault(){
        driver.switchTo().defaultContent();
    }

    public void returnToParent(){
        driver.switchTo().parentFrame();
    }

}
