package tests;

import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObject.BasePage;
import pageObject.HomePage;

import java.util.concurrent.TimeUnit;

public class HomeTestsDataProvider extends TestBase {

    WebDriver driver;

    @BeforeTest
    public void beforeTest(){
        driver = DriverManager.getWebDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().to("https://fakestore.testelka.pl/");
    }

    @AfterTest
    public void afterTest(){
        //driver.close(); //okno
        //driver.quit(); //sesja

    }


    @DataProvider(name = "searchDateProvider")
    public Object[][] createTable(){
        Object[][] table = {{"Egipt – El Gouna","Wyniki wyszukiwania dla „Egipt – El Gouna” – FakeStore"},
                {"Grecja – Limnos", "Wyniki wyszukiwania dla „Grecja – Limnos” – FakeStore"}};
        return  table;

    }

    @Test(dataProvider = "searchDateProvider")

    public void searchTest(String request, String response2){

        BasePage basePage = new BasePage();
        basePage.search.clear();
        basePage.search.sendKeys(request);
        basePage.search.submit();

        Assert.assertEquals(driver.getTitle(),response2,"Incorrect results");
    }


}
