package tests;

import driver.DriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class GoogleTests {

    WebDriver driver;

    @BeforeTest
    public void beforeTest(){
         driver = DriverManager.getWebDriver();
         driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
         driver.navigate().to("https://www.google.com/");
    }

    @AfterTest
    public void afterTest(){
        //driver.close(); //okno
        //driver.quit(); //sesja

    }


    @Parameters({"request","response"})
    @Test
    public void exampleTest(String request, String response1){
        WebElement inputField = driver.findElement(By.name("q"));

        inputField.clear();
        inputField.sendKeys(request);
        inputField.submit();

        Assert.assertEquals(driver.getTitle(),response1,"Incorrect title");



    }


}
