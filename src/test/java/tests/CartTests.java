package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObject.*;

public class CartTests extends TestBase {

    @Test
    public void addAndRemoveProduct(){
        BasePage basePage = new BasePage();
        basePage.closeCookieInfo();
        HomePage homePage = basePage.selectHomePage();
        Assert.assertTrue(homePage.isHomepageIsCorrectly());
        ClimbingCategoryPage climbingCategoryPage = homePage.goToClimbing();
        Assert.assertTrue(climbingCategoryPage.isClimbingCategoryPageDisplayCorrectly());
        CartPage cartPage = climbingCategoryPage.addProduct40ToCart().goToCart();
        Assert.assertTrue(cartPage.isCartpageDisplayCorrectly());
        Assert.assertEquals(cartPage.getNumberOfProducts(),"1", "Add more than 1 product to cart");
        cartPage.removeProduct();
        Assert.assertEquals(cartPage.infoAboutEmptyCart(), "Twój koszyk jest pusty.", "Don't remove product from cart.");
        //Assert.assertEquals();
                //.goToClimbing().addProduct40ToCart().goToCart().removeProduct();
    }

    @Test
    public void selectProduct() {
        BasePage basePage = new BasePage();
        //basePage.closeCookieInfo();
        HomePage homePage = basePage.selectHomePage();
        Assert.assertTrue(homePage.isHomepageIsCorrectly());
        ClimbingCategoryPage climbingCategoryPage = homePage.goToClimbing();
        Assert.assertTrue(climbingCategoryPage.isClimbingCategoryPageDisplayCorrectly());
        ProductPage productPage = climbingCategoryPage.selectProduct40();


    }



}
