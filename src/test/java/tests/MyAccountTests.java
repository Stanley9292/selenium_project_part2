package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObject.BasePage;
import pageObject.MyAccountPage;

public class MyAccountTests extends TestBase {

    String name = "stanislawtciesielka";

    @Test
    public void registerUser(){
        BasePage basePage = new BasePage();
        basePage.closeCookieInfo();
        MyAccountPage myAccountPage = basePage.selectMyAccountPage();
        Assert.assertTrue(myAccountPage.isMyAccountDisplayCorrectly());
        myAccountPage.registerOn();
        Assert.assertEquals(myAccountPage.getInfoAboutRegisterError(), "Błąd: Konto z tym adresem e-mail już istnieje. Zaloguj się.", "Enter data to new account");


    }

    @Test
    public void loginUser(){
       BasePage basePage = new BasePage();
       basePage.closeCookieInfo();
       MyAccountPage myAccountPage = basePage.selectMyAccountPage();
       Assert.assertTrue(myAccountPage.isMyAccountDisplayCorrectly());
       myAccountPage.logIn();
       Assert.assertEquals(myAccountPage.getTextAfterPositiveLogin(), "Witaj " + name + " (nie jesteś " +  name +"? Wyloguj się)", "Input negative data");
    }


}
