package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class ShopPage extends BasePage {

    @FindBy(css="*[class='woocommerce-products-header__title page-title']")
    private WebElement shopHeader;


    // first category
    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'windsurfing')]")
    private WebElement shop_windsurfing_category;

    // second category

    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'wspinaczka')]")
    private WebElement shop_climbing_category;

    // third category

    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'yoga')]")
    private WebElement shop_yoga_category;

    //fourth category

    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'zeglarstwo')]")
    private WebElement shop_sailing_category;


    public ShopPage() {
        super();

        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public boolean isShopPageDisplayCorrectly(){
        WaitForElement.waitForElementVisible(shopHeader,6);
        return shopHeader.equals("Sklep");
    }

    public WindsurfingCategoryPage selectWindsurfing() {
        shop_windsurfing_category.click();
        return new WindsurfingCategoryPage();


    }

    public ClimbingCategoryPage selectClimbing() {
        shop_climbing_category.click();
        return new ClimbingCategoryPage();

    }

    public YogaCategoryPage selectYoga(){
        shop_yoga_category.click();
        return new YogaCategoryPage();
    }

    public SailingCategoryPage selectSailing(){
        shop_sailing_category.click();
        return new SailingCategoryPage();


    }

    public String getShopHeader(){
        return shopHeader.getText();
    }
}
