package pageObject;


import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;


public class YogaCategoryPage extends BasePage{

    @FindBy(css="*[class = 'woocommerce-products-header__title page-title']")
    private WebElement yogaHeader;

    //Sort field
    @FindBy(className="orderby")
    private WebElement orderby_field;


    //Price_filtr
    @FindBy(className="price_slider_wrapper")
    private WebElement filtr_price;

    //Filtr submit
    @FindBy(xpath="//button[(text()='Filtruj')]")
    private WebElement filtr_submit;

    //Select  product 60
    @FindBy(xpath = "//li[contains(@class,'post-60')]/a[1]")
    private WebElement select__product_60;

    //Add to cart_product 60
    @FindBy(xpath = "//li[contains(@class,'post-60')]/a[2]")
    private WebElement add_to_cart_60;


    //See your cart
    @FindBy(css="*[class='added_to_cart wc-forward']")
    private WebElement see_user_cart;

    //Select  product 53
    @FindBy(xpath = "//li[contains(@class,'post-53')]/a[1]")
    private WebElement select__product_53;

    //Add to cart_product 53
    @FindBy(xpath = "//li[contains(@class,'post-53')]/a[2]")
    private WebElement add_to_cart_53;

    //Select  product 61
    @FindBy(xpath = "//li[contains(@class,'post-61')]/a[1]")
    private WebElement select__product_61;

    //Add to cart_product 61
    @FindBy(xpath = "//li[contains(@class,'post-61')]/a[2]")
    private WebElement add_to_cart_61;

    //Select  product 62
    @FindBy(xpath = "//li[contains(@class,'post-62')]/a[1]")
    private WebElement select__product_62;

    //Add to cart_product 62
    @FindBy(xpath = "//li[contains(@class,'post-62')]/a[2]")
    private WebElement add_to_cart_62;

    //Select  product 64
    @FindBy(xpath = "//li[contains(@class,'post-60')]/a[1]")
    private WebElement select__product_64;

    //Add to cart_product 64
    @FindBy(xpath = "//li[contains(@class,'post-64')]/a[2]")
    private WebElement add_to_cart_64;



    public YogaCategoryPage(){
        super();
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public boolean isYogaCategoryDisplayCorrectly(){
        WaitForElement.waitForElementVisible(yogaHeader,6);
        return getYogaHeader().equals("Yoga i pilates");
    }

    public YogaCategoryPage selectSortOption(){
        orderby_field.click();
        return this;
    }

    public YogaCategoryPage filtrSubmit(){
        filtr_submit.click();
        return this;
    }

    public CartPage goToCart() {
        see_user_cart.click();
        return new CartPage();
    }

    public ProductPage selectProduct53(){
        select__product_53.click();
        return new ProductPage();
    }

    public YogaCategoryPage addProduct53ToCart(){
        add_to_cart_53.click();
        return this;
    }

    public ProductPage selectProduct60(){
        select__product_60.click();
        return new ProductPage();
    }

    public YogaCategoryPage addProduct60ToCart(){
        add_to_cart_60.click();
        return this;
    }

    public ProductPage selectProduct61(){
        select__product_61.click();
        return new ProductPage();
    }

    public YogaCategoryPage addProduct61ToCart(){
        add_to_cart_61.click();
        return this;
    }

    public ProductPage selectProduct62(){
        select__product_62.click();
        return new ProductPage();
    }

    public YogaCategoryPage addProduct62ToCart(){
        add_to_cart_62.click();
        return this;
    }

    public ProductPage selectProduct64(){
        select__product_64.click();
        return new ProductPage();
    }

    public YogaCategoryPage addProduct64ToCart(){
        add_to_cart_64.click();
        return this;
    }

    public String getYogaHeader(){
        return yogaHeader.getText();
    }






}


