package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class SailingCategoryPage {


    @FindBy(css="*[class='woocommerce-products-header__title page-title']")
    private WebElement sailingHeader;


    //See your cart
    @FindBy(css="*[class='added_to_cart wc-forward']")
    private WebElement see_user_cart;


    //Sort field
    @FindBy(className="orderby")
    private WebElement orderby_field;


    //Price_filtr
    @FindBy(className="price_slider_wrapper")
    private WebElement filtr_price;

    //Filtr submit
    @FindBy(xpath="//button[(text()='Filtruj')]")
    private WebElement filtr_submit;


    //Select product 46
    @FindBy(xpath = "//li[contains(@class,'post-46')]/a[1]")
    private WebElement select_product_46;

    //Add to cart product 46
    @FindBy(xpath ="//li[contains(@class,'post-46')]/a[2]")
    private WebElement add_to_cart_46;


    public SailingCategoryPage(){
        super();
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public boolean isSailingCategoryDisplayCorrect(){
        WaitForElement.waitForElementVisible(sailingHeader,6);
        return getSailingHeader().equals("Żeglarstwo");
    }


    public SailingCategoryPage selectSortOption(){
        orderby_field.click();
        return this;
    }

    public SailingCategoryPage filtrSubmit(){
        filtr_submit.click();
        return this;
    }

    public CartPage goToCart() {
        see_user_cart.click();
        return new CartPage();
    }

    public ProductPage selectProduct46(){
        select_product_46.click();
        return new ProductPage();
    }

    public SailingCategoryPage addProduct46ToCart(){
        add_to_cart_46.click();
        return this;
    }

    public String getSailingHeader(){
        return sailingHeader.getText();
    }

}
