package pageObject;


import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage extends BasePage {


    //SearchPage header
    @FindBy(xpath = "//h1[contains(text(), 'Wyniki wyszukiwania')]")
    private WebElement search_header;


    public SearchPage() {
        super();

        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public SearchPage fetchSearchHeader() {
        search_header.getText();
        return this;
    }


}



