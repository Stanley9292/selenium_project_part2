package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;


public class ClimbingCategoryPage extends BasePage {

    //Sort field
    @FindBy(className = "orderby")
    private WebElement orderby_field;

    @FindBy(css="*[class = 'woocommerce-products-header__title page-title']")
    private WebElement header_climbing;


    //Price_filtr
    @FindBy(className = "price_slider_wrapper")
    private WebElement filtr_price;

    //Filtr submit
    @FindBy(xpath = "//button[(text()='Filtruj')]")
    private WebElement filtr_submit;

    //Select first product
    @FindBy(xpath = "//li[contains(@class,'post-42')]/a[1]")
    private WebElement select_first_product_climbing_42;

    //Add to cart_first_product
    @FindBy(xpath = "//li[contains(@class,'post-42')]/a[2]")
    private WebElement add_to_cart_first_climbing;


    //See your cart
    @FindBy(css = "*[class='added_to_cart wc-forward']")
    private WebElement see_user_cart;

    //Select second product
    @FindBy(xpath = "//li[contains(@class,'post-40')]/a[1]")
    private WebElement select_second_product_climbing_40;

    //Add to cart_second_product
    @FindBy(xpath = "//li[contains(@class,'post-40')]/a[2]")
    private WebElement add_to_cart_second_climbing;


    public ClimbingCategoryPage() {
        super();
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public boolean isClimbingCategoryPageDisplayCorrectly() {
        WaitForElement.waitForElementVisible(header_climbing,6);
        return headerClimbing().equals("Wspinaczka");
    }



    public ClimbingCategoryPage selectSortOption(){
        orderby_field.click();
        return  this;
    }

    public ClimbingCategoryPage filtrSubmit(){
        filtr_submit.click();
        return this;
    }

    public CartPage goToCart() {
        WaitForElement.waitForElementVisible(see_user_cart, 6);
        see_user_cart.click();
        return new CartPage();
    }

    public ProductPage selectProduct40(){
        select_second_product_climbing_40.click();
        return new ProductPage();
    }

    public ClimbingCategoryPage addProduct40ToCart(){
        add_to_cart_second_climbing.click();
        return this;
    }

    public ProductPage selectProduct42(){
        select_first_product_climbing_42.click();
        return new ProductPage();
    }

    public ClimbingCategoryPage addProduct42ToCart(){
        add_to_cart_first_climbing.click();
        return this;
    }

    public String headerClimbing(){
        return header_climbing.getText();
    }


}
