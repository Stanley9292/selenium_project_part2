package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utiles.Randomizer;
import waits.WaitForElement;

public class MyAccountPage extends BasePage {

    //************************************Login Area********************

    //loginUser

    @FindBy(id = "username")
    private WebElement login_username;

    @FindBy(className = "entry-title")
    private WebElement myAccountHeader;

    //loginPassword
    @FindBy(id = "password")
    private WebElement login_pass;

    // checkbox remember me
    @FindBy(id = "rememberme")
    private WebElement login_checkbox_remember_me;

    //button login

    @FindBy(name = "login")
    private WebElement login_submit;

    // lost password

    @FindBy(css = "*[class='woocommerce-LostPassword lost_password']>a")
    private WebElement login_lost_password;


//************************************Register Area********************

//register email

    @FindBy(id = "reg_email")
    private WebElement register_email;

//register password

    @FindBy(id = "reg_password")
    private WebElement register_password;

    //register submit
    @FindBy(name = "register")
    private WebElement register_submit;

    //If account exist
    @FindBy(className= "woocommerce-error")
    private WebElement info_about_register_error;

    //Info after login
    @FindBy(xpath = "//*[@class ='woocommerce-MyAccount-content']/p")
    private WebElement info_after_positive_login;




    public MyAccountPage() {

        PageFactory.initElements(DriverManager.getWebDriver(), this);

    }

    public boolean isMyAccountDisplayCorrectly(){
        WaitForElement.waitForElementVisible(myAccountHeader,6);
        return getMyAccountHeader().equals("Moje konto");
    }

    public MyAccountPage logIn() {
        login_username.clear();
        login_username.sendKeys(Randomizer.email());
        login_pass.clear();
        login_pass.sendKeys(Randomizer.password());
        login_submit.click();
        return this;
    }

//    public void enterLoginUsername(String user) {
//        login_username.clear();
//        login_username.sendKeys(user);
//    }
//
//    public void enterLoginUserPass(String pass) {
//        login_pass.clear();
//        login_pass.sendKeys(pass);
//
//    }
//
//    public void submitLogin() {
//        login_submit.click();
//    }
//
//    public void logIn(String username, String password) {
//        this.enterLoginUsername("stanislawciesielka@gmail.com");
//        this.enterLoginUserPass("Stachu1992!");
//        this.submitLogin();
//
//
//    }

    public String getTextAfterPositiveLogin(){
        return info_after_positive_login.getText();
    }

    public MyAccountPage rememberMe() {
        login_checkbox_remember_me.click();
        return this;
    }

    public RemindPasswordPage remindPassword() {
        login_lost_password.click();
        return new RemindPasswordPage();
    }

//    public void enterRegisterUsername(String user){
//        register_email.clear();
//        register_email.sendKeys(user);
//    }
//
//    public void enterRegisterUserPass(String pass){
//        register_password.clear();
//        register_password.sendKeys(pass);
//
//    }
//
//    public void submitRegister(){
//        register_submit.click();
//    }
//
//    public void registerIn(String username, String password){
//        this.enterRegisterUsername(username);
//        this.enterRegisterUserPass(password);
//        this.submitRegister();
//
//
//    }

    public MyAccountPage registerOn() {
        WaitForElement.waitForElementVisible(register_email, 5);
        register_email.clear();
        register_email.sendKeys(Randomizer.email());
        register_password.clear();
        register_password.sendKeys(Randomizer.password());
        register_submit.click();
        return this;

    }

    public String getMyAccountHeader(){
        return myAccountHeader.getText();
    }

    public String getInfoAboutRegisterError(){
        WaitForElement.waitForElementVisible(info_about_register_error,10);
        return info_about_register_error.getText();
    }
}









