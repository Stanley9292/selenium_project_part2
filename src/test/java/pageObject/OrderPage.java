package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import utiles.IframeMenager;
import utiles.Randomizer;
import waits.WaitForElement;

import java.util.List;

public class OrderPage extends BasePage {

    // *************************************Login area***************************************

    // show login area
    @FindBy(className = "showlogin")
    private WebElement showlogin;

    @FindBy(className = "entry-title")
    private WebElement orderHeader;

    //loginxUser

    @FindBy(id = "username")
    private WebElement login_username;

    //loginPassword
    @FindBy(id = "password")
    private WebElement login_pass;

    // checkbox remember me
    @FindBy(id = "rememberme")
    private WebElement login_checkbox_remember_me;

    //button login

    @FindBy(name = "login")
    private WebElement login_submit;

    // lost password

    @FindBy(css = "*[class='lost_password']>a")
    private WebElement login_lost_password;

    // *************************************Coupon area***************************************

    // show coupon area
    @FindBy(className = "showcoupon")
    private WebElement coupon_show;

    //coupon code
    @FindBy(id = "coupon_code")
    private WebElement coupon_code;

    //coupon apply
    @FindBy(name = "apply_coupon")
    private WebElement coupon_apply;

    // *************************************Billing area***************************************

    //billing first name
    @FindBy(id = "billing_first_name")
    private WebElement billing_first_name;

    //billing last name
    @FindBy(id = "billing_last_name")
    private WebElement billing_last_name;

    //billing_company
    @FindBy(id = "billing_company")
    private WebElement billing_company;

    //billing_adress_1
    @FindBy(id = "billing_address_1")
    private WebElement billing_address;


    //billing_postcode
    @FindBy(id = "billing_postcode")
    private WebElement billing_postcode;

    //billing_city
    @FindBy(id = "billing_city")
    private WebElement billing_city;

    //billing_phone
    @FindBy(id = "billing_phone")
    private WebElement billing_phone;


    //billing_email
    @FindBy(id = "billing_email")
    private WebElement billing_email;

    //create account
    @FindBy(id = "createaccount")
    private WebElement billing_create_account;


    //billing_password
    @FindBy(id = "account_password")
    private WebElement billingPassword;

    //order comments

    @FindBy(id = "order_comments")
    private WebElement order_comments;

    //*******************************************************************Card details**********************

    //Card Number
    @FindBy(name = "cardnumber")
    private WebElement card_number;

    // @FindBys(name=)
    // private List<WebElement>

    //Expired Date
    @FindBy(name = "exp-date")
    private WebElement card_expired_date;

    //CVC code
    @FindBy(name = "cvc")
    private WebElement card_cvc_code;

    //Ccheckbox future order
    @FindBy(name = "id='wc-stripe-new-payment-method")
    private WebElement payment_method_for_future_order;

    //Check terms

    @FindBy(id = "terms")
    private WebElement check_terms;


    //Place order
    @FindBy(id = "place_order")
    private WebElement place_order;

    //Shop Table
    @FindBy(xpath = "//table[contains(@class,'shop_table')]")
    private WebElement shopTable;


    public OrderPage() {
        super();

        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public boolean isOrderDisplayCorrectly() {
        WaitForElement.waitForElementVisible(orderHeader, 6);
        return getOrderHeader().equals("Zamówienie");
    }

    public OrderPage showLogAreaAndLogIn(String username, String password) {
        showlogin.click();
        login_username.clear();
        login_username.sendKeys(username);
        login_pass.clear();
        login_pass.sendKeys(password);
        login_submit.click();
        return this;


    }

    public OrderPage rememberMe() {
        login_checkbox_remember_me.click();
        return this;
    }

    public RemindPasswordPage remindPassword() {
        login_lost_password.click();
        return new RemindPasswordPage();
    }

    public OrderPage insertCouponArea(String code) {
        coupon_show.click();
        coupon_code.clear();
        coupon_code.sendKeys(code);
        coupon_apply.click();
        return this;
    }

    public OrderPage insertDataCash() {
        billing_first_name.clear();
        billing_first_name.sendKeys(Randomizer.firstName());
        billing_last_name.clear();
        billing_last_name.sendKeys(Randomizer.lastName());
        billing_company.clear();
        billing_company.sendKeys(" ");
        billing_address.clear();
        billing_address.sendKeys(Randomizer.fullAdress());
        billing_postcode.clear();
        billing_postcode.sendKeys(Randomizer.postcode());
        billing_city.clear();
        billing_city.sendKeys(Randomizer.city());
        billing_phone.clear();
        billing_phone.sendKeys(Randomizer.phoneNumber());
        billing_email.clear();
        billing_email.sendKeys(Randomizer.email());
        return this;

    }

    public OrderPage createAccount() {
        billing_create_account.click();
        billingPassword.clear();
        billingPassword.sendKeys(Randomizer.password());
        return this;
    }

    public OrderPage addOrderMessage() {
        order_comments.clear();
        order_comments.sendKeys(" ");
        return this;
    }

    public OrderPage insertCardDate() {
        OrderPage orderPage = new OrderPage();
        orderPage.findElementInFrame("__privateStripeFrame8", card_number);
        orderPage.addCardNumber();
        orderPage.findElementInFrame("__privateStripeFrame9", card_expired_date);
        orderPage.addExpiredDate();
        orderPage.findElementInFrame("__privateStripeFrame10", card_cvc_code);
        orderPage.addCVCCode();
        IframeMenager iframeMenager = new IframeMenager();
        iframeMenager.returnToDefault();

        return this;
    }



    public OrderPage addCardNumber() {
        card_number.click();
        card_number.clear();
        Actions actions = new Actions(DriverManager.getWebDriver());
        actions.sendKeys(card_number,createCardNumber()).perform();
        return this;
    }

    public String createCardNumber(){
        String card = " ";
        for(int i = 0; i < 8; i++){
            card+="42";
        }
        return card;

    }

    public OrderPage addCVCCode() {
        card_cvc_code.clear();
        card_cvc_code.sendKeys(Randomizer.cvccode());
        return this;
    }

    public OrderPage addExpiredDate() {
        card_expired_date.clear();
        card_expired_date.sendKeys(Randomizer.expiriedDate());
        return this;
    }

   /*public OrderPage addCardNumber(){
        WaitForElement.waitForElementVisible(shopTable);
        IframeMenager iframeMenager = new IframeMenager();
        iframeMenager.changeIframe("__privateStripeFrame8");
        WaitForElement.waitForElementVisible(card_number);
        card_number.clear();
        card_number.sendKeys("4242424242424242");
        iframeMenager.returnToDefault();
        return this;
    }*/



    /*public OrderPage addExpiredDate(){
        IframeMenager iframeMenager = new IframeMenager();
        iframeMenager.changeIframe("__privateStripeFrame9");
        WaitForElement.waitForElementVisible(card_expired_date);
        card_expired_date.clear();
        card_expired_date.sendKeys("0522");
        iframeMenager.returnToDefault();
        return this;
    }*/

    /*public OrderPage addCVCCode(){
        IframeMenager iframeMenager = new IframeMenager();
        iframeMenager.changeIframe("__privateStripeFrame10");
        WaitForElement.waitForElementVisible(card_cvc_code);
        card_cvc_code.clear();
        card_cvc_code.sendKeys("123");
        iframeMenager.returnToDefault();
        return this;
    }*/


    /*public OrderPage insertCardData() {
        addCardNumber();
        addExpiredDate();
        addCVCCode();
        return this;

    }*/

    public OrderPage buyAndPayOrder() {

        WaitForElement.waitForElementClickable(check_terms);
        check_terms.click();
        place_order.click();
        return this;

    }

    public String getOrderHeader() {
        return orderHeader.getText();
    }


    public OrderPage findElementInFrame(String frameLocator, WebElement element) {
        IframeMenager iframeMenager = new IframeMenager();
        iframeMenager.returnToDefault();
        WaitForElement.waitFrameToBeAvailableAndSwitchToIt(frameLocator);
        WaitForElement.waitForElementClickable(element);
        return this;


    }

}


