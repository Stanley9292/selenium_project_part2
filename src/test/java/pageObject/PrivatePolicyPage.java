package pageObject;


import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PrivatePolicyPage extends BasePage {

//PrivatePolicy header
@FindBy(className = "entry-header")
private WebElement private_policy_header;


public PrivatePolicyPage(){
        super();

        PageFactory.initElements(DriverManager.getWebDriver(),this);
        }

public PrivatePolicyPage fetchPrivatePolicyHeader(){
        private_policy_header.getText();
        return this;
        }






}
