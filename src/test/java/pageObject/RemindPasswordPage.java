package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RemindPasswordPage extends BasePage {

    @FindBy(id="user_login")
    private WebElement user_login;

    @FindBy(css="*[class='woocommerce-Button button']")
    private WebElement submit_remind;

    public RemindPasswordPage(){
        super();
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public RemindPasswordPage inputUserNameAndRemindPass(String userlogin){
        user_login.clear();
        user_login.sendKeys(userlogin);
        submit_remind.click();
        return this;
    }
}
