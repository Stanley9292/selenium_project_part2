package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

import java.util.List;

public class CartPage extends BasePage {

    //cart_remove_product
    @FindBy(className = "remove")
    private WebElement cart_remove_product;

    @FindBy(className = "entry-title")
    private WebElement cartpage_header;

    //cart_info_about_remove_product
    @FindBy(xpath = "//*[@class='woocommerce-message' and contains(text(),'Usunięto')]")
    private WebElement cart_info_about_remove_product;


    //restore item
    @FindBy(css = "a[class='restore-item']")
    private WebElement info_restore_item;

    //empty_cart_info
    @FindBy(xpath = "//*[contains(@class,'cart-empty')]")
    private WebElement info_empty_cart;

    //coupon code
    @FindBy(id = "coupon_code")
    private WebElement cart_coupon_code;

    //apply_coupon
    @FindBy(name = "apply_coupon")
    private WebElement cart_apply_coupon;

    //info about coupon
    @FindBy(className = "woocommerce-error")
    private WebElement info_about_coupon;

    //updtade cart
    @FindBy(name = "update_cart")
    private WebElement cart_update_cart;

    //amountOfProduct
    @FindBy(css = "*[type='number']")
    private WebElement inputAmountOfProducts;

    //go to cash
    @FindBy(css = "*[class='checkout-button button alt wc-forward']")
    private WebElement cart_go_to_cash;

    //back_to_shop
    @FindBy(css = "*[class='button wc-backward']")
    private WebElement back_to_shop_button;




    @FindBys
            ({
                    @FindBy(xpath = "//*[contains(@class,'woocommerce-cart-form__cart-item')]")
            })
    private List<WebElement> InCartProducts;


    public CartPage() {
        super();

        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public boolean isCartpageDisplayCorrectly(){
        WaitForElement.waitForElementVisible(cartpage_header,6);
        return headerGetText().equals("Koszyk");
    }

    public CartPage removeProduct() {
        cart_remove_product.click();
        return this;
    }

    public CartPage infoAboutRemoveProduct() {
        cart_info_about_remove_product.getText();
        return this;

    }

    public CartPage restoreProduct() {
        info_restore_item.click();
        return this;
    }

    public String infoAboutEmptyCart() {
        WaitForElement.waitForElementVisible(info_empty_cart,7);

        return info_empty_cart.getText();
    }

    public CartPage clearAndInsertCoupon(String coupon) {
        cart_coupon_code.clear();
        cart_coupon_code.sendKeys(coupon);
        cart_apply_coupon.click();
        info_about_coupon.getText();
        return this;


    }

    public CartPage selectNumberProductAndUptdateCart(String number) {
        inputAmountOfProducts.clear();
        inputAmountOfProducts.sendKeys(number);
        cart_update_cart.click();
        return this;

    }

    public OrderPage goToCash() {
        cart_go_to_cash.click();
        return new OrderPage();
    }

    public ShopPage backToShop() {
        back_to_shop_button.click();
        return new ShopPage();
    }

    public String headerGetText(){
        return cartpage_header.getText();
    }

    public String getNumberOfProducts(){
        return inputAmountOfProducts.getAttribute("value");
    }

    public int cartSize(){
        return InCartProducts.size();
    }




}
