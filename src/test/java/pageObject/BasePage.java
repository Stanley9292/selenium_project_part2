package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class BasePage {

    // Info about fakestore - Down page
    @FindBy(className = "woocommerce-store-notice__dismiss-link")
    public WebElement info_about_fakestore;

    //Logo - fakestore
    @FindBy(className = "custom-logo-link")
    public WebElement logo_fakestore;

    //Search
    @FindBy(id = "woocommerce-product-search-field-0")
    public WebElement search;

    //*****************************Cart Menu******************************

    //Cart on right sight
    @FindBy(className = "cart-contents")
    public WebElement cart_right_side;

    @FindBy(xpath = "//a[@class='cart-contents']/*[@class='count']")
    public WebElement amountProductsInCart;

    //See cart
    @FindBy(css = "a[class='button wc-forward']")
    public WebElement see_cart;

    //See order
    @FindBy(css = "a[class='button checkout wc-forward']")
    public WebElement see_order;


    //**************************Main Menu**************************************************************

    //HomePage button on Main Menu
    @FindBy(xpath = "//a[text()='Strona główna' and @aria-current='page']")
    public WebElement menu_homepage;


    //Shop button on Main Menu
    @FindBy(xpath = "//a[text()='Sklep']")
    public WebElement menu_shop;

    //Order button on Main Menu
    @FindBy(xpath = "//a[text()='Zamówienie']")
    public WebElement menu_order;

    //Cage button on Main Menu
    @FindBy(xpath = "//a[text()='Koszyk']")
    public WebElement menu_cart;

    //My account button on Main Menu
    @FindBy(xpath = "//a[text()='Moje konto']")
    public WebElement menu_my_account;

    //List of wishes button on Main Menu
    @FindBy(xpath = "//a[@rel='noopener noreferrer']")
    public WebElement menu_list_of_wishes;

    //**************************Footer**************************************************************

    //Privacy Policy on Footer
    @FindBy(className = "privacy-policy-link")
    public WebElement footer_privacy_policy;

    //Info about technology on Footer
    @FindBy(xpath = "//a[text()='Zbudowany za pomocą Storefront i WooCommerce']")
    public WebElement footer_info_technology;

    //************************************Category Menu*********************************

    //Windsurfing Category
    @FindBy(xpath = "//a[text()='Windsurfing']")
    public WebElement category_windsurfing;

    //Climbing Category
    @FindBy(xpath = "//a[text()='Wspinaczka']")
    public WebElement category_climbing;

    //Yoga and pilates Category
    @FindBy(xpath = "//a[text()='Yoga i pilates']")
    public WebElement category_yoga;

    //Sailing Category

    @FindBy(xpath = "//a[text()='Żeglarstwo']")
    public WebElement category_sailing;


    public BasePage() {

        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }


    public HomePage closeCookieInfo() {
        info_about_fakestore.click();
        return new HomePage();
    }

    public HomePage clickMainLogo() {
        logo_fakestore.click();
        return new HomePage();

    }

    public void inputSearch(String sentence) {
        search.sendKeys(sentence);

    }

    public String getAmountOfProductsInCart(){
        WaitForElement.waitForElementVisible(amountProductsInCart,15);
        return amountProductsInCart.getText();
    }

    public void clickSearch() {
        search.click();
    }

    public SearchPage actionsInSearch(String sentence) {
        search.clear();
        inputSearch(sentence);
        clickSearch();
        return new SearchPage();

    }

    public CartPage clickCartInBase() {
        cart_right_side.click();
        return new CartPage();
    }

    public CartPage seeCart() {
        see_cart.click();
        return new CartPage();
    }

    public OrderPage seeOrder() {
        see_order.click();
        return new OrderPage();
    }

    public HomePage selectHomePage() {
        menu_homepage.click();
        return new HomePage();

    }

    public ShopPage selectShopPage() {
        menu_shop.click();
        return new ShopPage();

    }

    public OrderPage selectOrderPage() {
        menu_order.click();
        return new OrderPage();

    }

    public CartPage selectCartPage() {
        menu_cart.click();
        return new CartPage();
    }

    public MyAccountPage selectMyAccountPage() {
        menu_my_account.click();
        return new MyAccountPage();
    }

    public ShopPage clickShop() {
        menu_shop.click();
        return new ShopPage();

    }

    public ListOfWishesPage clickWishList() {
        menu_list_of_wishes.click();
        return new ListOfWishesPage();

    }

    public PrivatePolicyPage clickPrivatePolicyInFooter() {
        footer_privacy_policy.click();
        return new PrivatePolicyPage();

    }

    public TechnologyPage checkTechnology() {
        footer_info_technology.click();
        return new TechnologyPage();
    }

    public WindsurfingCategoryPage selectWindsurfingCategoryInRightMenu() {
        category_windsurfing.click();
        return new WindsurfingCategoryPage();
    }

    public ClimbingCategoryPage selectClimbingCategoryInRightMenu() {
        category_climbing.click();
        return new ClimbingCategoryPage();
    }

    public YogaCategoryPage selectYogaCategoryInRightMenu() {
        category_yoga.click();
        return new YogaCategoryPage();
    }

    public SailingCategoryPage selectSailingCategoryInRightMenu() {
        category_sailing.click();
        return new SailingCategoryPage();
    }

    public BasePage selectSearch(){
        search.click();
        return new BasePage();
    }



}

