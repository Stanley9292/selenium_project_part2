package pageObject;

import driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class HomePage extends BasePage {

    //Slider
    @FindBy(css = "img[class='slider-231 slide-232']")
    private WebElement slider;

    @FindBy(className = "entry-title")
    private WebElement homepage_header;

    //See your cage
    @FindBy(css = "*[class='added_to_cart wc-forward']" )
    private WebElement see_your_cage;

    //********************************************Buy_Categories****************************************************************

    //Buy_first_category
    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'windsurfing')]")
    private WebElement buy_first_category;

    //Buy_second_category
    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'wspinaczka')]")
    private WebElement buy_second_category;

    //Buy last category
    @FindBy(xpath = "//li[contains(@class,'product-category')]/a[contains(@href,'yoga')]")
    private WebElement buy_last_category;

    //********************************************Buy_New****************************************************************

    //New  Product 393
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-393')]/a[1]" )
    private WebElement new__product_393;

    //Add Cart New Product 393
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-393')]/a[2]" )
    private WebElement add_new_product_393;

    //New  Product 386
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-386')]/a[1]" )
    private WebElement new__product_386;

    //Add Cart New Product 386
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-386')]/a[2]" )
    private WebElement add_new_product_386;

    //New  Product 391
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-391')]/a[1]" )
    private WebElement new__product_391;

    //Add Cart New Product 391
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-391')]/a[2]" )
    private WebElement add_new_product_391;

    //New  Product 389
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-389')]/a[1]" )
    private WebElement new__product_389;

    //Add Cart New Product 389
    @FindBy(xpath = "//h2[text()='Nowości']/..//li[contains(@class,'post-389')]/a[2]" )
    private WebElement add_new_product_389;
    //*****************************************************Buy_Popular******************************************

    //New  Product 393
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-393')]/a[1]" )
    private WebElement popular_product_393;

    //Add Cart New Product 393
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-393')]/a[2]" )
    private WebElement add_popular_product_393;

    //New  Product 386
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-386')]/a[1]" )
    private WebElement popular__product_386;

    //Add Cart New Product 386
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-386')]/a[2]" )
    private WebElement add_popular_product_386;

    //New  Product 391
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-391')]/a[1]" )
    private WebElement popular__product_391;

    //Add Cart New Product 391
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-391')]/a[2]" )
    private WebElement add_popular_product_391;

    //New  Product 389
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-389')]/a[1]" )
    private WebElement popular__product_389;

    //Add Cart New Product 389
    @FindBy(xpath = "//h2[text()='Popularne']/..//li[contains(@class,'post-389')]/a[2]" )
    private WebElement add_popular_product_389;
    //*****************************************************Buy_Promotion******************************************

    //Promotion First Product
    @FindBy(xpath = "//h2[text()='Prommocja']/..//li[contains(@class,'post-40')]/a[1]" )
    private WebElement promotion__product_40;

    //Add Cage Promotion First Product
    @FindBy(xpath = "//h2[text()='Promocja']/..//li[contains(@class,'post-40')]/a[2]" )
    private WebElement add_promotion_product_40;

    //Promotion First Product
    @FindBy(xpath = "//h2[text()='Prommocja']/..//li[contains(@class,'post-46')]/a[1]" )
    private WebElement promotion__product_46;

    //Add Cage Promotion First Product
    @FindBy(xpath = "//h2[text()='Promocja']/..//li[contains(@class,'post-46')]/a[2]" )
    private WebElement add_promotion_product_46;

    //Promotion First Product
    @FindBy(xpath = "//h2[text()='Prommocja']/..//li[contains(@class,'post-53')]/a[1]" )
    private WebElement promotion__product_53;

    //Add Cage Promotion First Product
    @FindBy(xpath = "//h2[text()='Promocja']/..//li[contains(@class,'post-53')]/a[2]" )
    private WebElement add_promotion_product_53;

    //Promotion First Product
    @FindBy(xpath = "//h2[text()='Prommocja']/..//li[contains(@class,'post-61')]/a[1]" )
    private WebElement promotion__product_61;

    //Add Cage Promotion First Product
    @FindBy(xpath = "//h2[text()='Promocja']/..//li[contains(@class,'post-61')]/a[2]" )
    private WebElement add_promotion_product_61;

    //*****************************************************Buy_Best******************************************

    //best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-60')]/a[1]" )
    private WebElement best_product_60;

    //Add Cage Best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-60')]/a[2]" )
    private WebElement add_best_product_60;

    //best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-61')]/a[1]" )
    private WebElement best_product_61;

    //Add Cage Best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-61')]/a[2]" )
    private WebElement add_best_product_61;

    //best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-386)]/a[1]" )
    private WebElement best_product_386;

    //Add Cage Best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-386')]/a[2]" )
    private WebElement add_best_product_386;

    //best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-393')]/a[1]" )
    private WebElement best_product_393;

    //Add Cage Best First Product
    @FindBy(xpath = "//h2[text()='Bestsellery']/..//li[contains(@class,'post-393')]/a[2]" )
    private WebElement add_best_product_393;

    //Loading icon
    @FindBy(css=".blockOverlay")
    private WebElement loadingIcon;



    public HomePage(){
        super();

        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public boolean isHomepageIsCorrectly(){
        WaitForElement.waitForElementVisible(homepage_header, 6);
        return getText().contains("podróż dla siebie!");
    }

    public void moveToCart(){
        Actions action = new Actions(DriverManager.getWebDriver());
        action.moveToElement(see_your_cage).moveToElement(see_cart).click(see_cart).build().perform();

    }

    public CartPage seeYourCart(){
        see_your_cage.click();
        return new CartPage();
    }

    public WindsurfingCategoryPage goToWindsurfing(){
        buy_first_category.click();
        return new WindsurfingCategoryPage();
    }

    public ClimbingCategoryPage goToClimbing(){
        WaitForElement.waitForElementClickable(buy_second_category);
        buy_second_category.click();
        return new ClimbingCategoryPage();
    }

    public YogaCategoryPage goToYoga(){
        buy_last_category.click();
        return new YogaCategoryPage();

    }
    public ProductPage select386FromNew(){
        new__product_386.click();
        return new ProductPage();
    }

    public HomePage addProduct386ToCart(){
        add_new_product_386.click();
        return this;
    }


    public ProductPage select389FromNew(){
        new__product_389.click();
        return new ProductPage();
    }

    public HomePage addProduct389ToCart(){
        add_new_product_389.click();
        return this;
    }

    public ProductPage select391FromNew(){
        new__product_391.click();
        return new ProductPage();
    }

    public HomePage addProduct391ToCart(){
        add_new_product_391.click();
        return this;
    }

    public ProductPage select393FromNew(){
        new__product_393.click();
        return new ProductPage();
    }

    public HomePage addProduct393ToCart(){
        add_new_product_393.click();
        return this;
    }

    public ProductPage select386FromPopular(){
        popular__product_386.click();
        return new ProductPage();
    }

    public HomePage addProduct386FromPopularToCart(){
        add_popular_product_386.click();
        return this;
    }

    public ProductPage select389FromPopular(){
        popular__product_389.click();
        return new ProductPage();
    }

    public HomePage addProduct389FromPopularToCart(){
        add_popular_product_389.click();
        return this;
    }

    public ProductPage select391FromPopular(){
        popular__product_391.click();
        return new ProductPage();
    }

    public HomePage addProduct391FromPopularToCart(){
        add_popular_product_391.click();
        return this;
    }

    public ProductPage select393FromPopular(){
        popular_product_393.click();
        return new ProductPage();
    }

    public HomePage addProduct393FromPopularToCart(){
        add_popular_product_393.click();
        return this;
    }


    //******************************************************************
    public ProductPage select40FromPromotion(){
        promotion__product_40.click();
        return new ProductPage();
    }

    public HomePage addProduct40FromPromotionToCart(){
        add_promotion_product_40.click();
        return this;
    }


    public ProductPage select46FromPromotion(){
        promotion__product_46.click();
        return new ProductPage();
    }

    public HomePage addProduct46FromPromotionToCart(){
        add_promotion_product_46.click();
        return this;
    }

    public ProductPage select53FromPromotion(){
        promotion__product_53.click();
        return new ProductPage();
    }

    public HomePage addProduct53FromPromotionToCart(){
        add_promotion_product_53.click();
        return this;
    }

    public ProductPage select61FromPromotion(){
        promotion__product_61.click();
        return new ProductPage();
    }

    public HomePage addProduct61FromPromotionToCart(){
        add_promotion_product_61.click();
        return this;
    }

    //******************************************************

    public ProductPage select61FromBest(){
        best_product_61.click();
        return new ProductPage();
    }

    public HomePage addProduct61FromBestToCart(){
        add_best_product_61.click();
        return this;
    }

    public ProductPage select60FromBest(){
        best_product_60.click();
        return new ProductPage();
    }

    public HomePage addProduct60FromBestToCart(){
        add_best_product_60.click();
        return this;
    }

    public ProductPage select386FromBest(){
        best_product_386.click();
        return new ProductPage();
    }

    public HomePage addProduct386FromBestToCart(){
        add_best_product_386.click();
        return this;
    }

    public ProductPage select393FromBest(){
        best_product_393.click();
        return new ProductPage();
    }

    public HomePage addProduct393FromBestToCart(){
        add_best_product_393.click();
        return this;
    }

    public String getText(){
        return homepage_header.getText();
    }















}
